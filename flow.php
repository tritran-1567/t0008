<?php $id="flow";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<div class="p-flow">
    <main class="l-container">
        <div class="l-row">
            <div class="c-breadcrumb">
                <a href="/">トップページ</span></a><a href="./flow.php">住まいづくりのすすめ方</a>
            </div>
            <!-- p-flow1 -->
            <section class="p-flow1">
                <img src="./assets/image/flow/title.png" alt="" class="c-flow__title">
                <img src="./assets/image/flow/img1.jpg" alt="" class="c-flow__step">
                <div class="c-flow__content u-center">
                    住宅にせよ、業務用の建物にせよ、初めての方が大半だと思います。<br/>
                    「何から始めて良いか？」わからないのは当然です。<br/>
                    そういった時に、最初に相談する窓口が我々「建築設計事務所」です。<br/>
                    一級建築士事務所SHEaPでは、建物のことだけでなく、<br/>
                    住まいづくりにおける資金計画や土地のこと、<br/>
                    事業用の土地や運営計画まで、<br/>
                    建築に関わる初期段階の相談を承っております。<br/>
                    気軽にお声かけください。
                   <p>下記に、住宅の場合を例に大まかなスケジュールを表示しましたので、ごらんください。 </p>
                </div>
            </section>
            <!-- p-flow2 -->
            <section class="p-flow2">
                <div class="c-step__item">
                    <div class="c-step__title u-right">
                        <img src="./assets/image/flow/title1.png" alt="">
                    </div>
                    <div class="c-step__inner">
                        <div class="c-imgText1">
                            <div class="c-imgText1__img">
                                <img src="./assets/image/flow/step1.jpg" alt="">
                            </div>
                            <div class="c-imgText1__content">
                                まずは、お気軽にご連絡ください。<br/>
                                実際の暮らしを体感していただくことで、間取り、光や風<br/>
                                の入り方、生活動線などの空間イメージや設備機器、建材<br/>
                                などの素材のイメージの参考にしていただきやすいかと思<br/>
                                います。<br/>
                                自分らしい住まいづくりのスタートです。
                            </div>
                        </div>
                    </div>
                </div>
                <div class="c-step__item">
                    <div class="c-step__title u-right">
                        <img src="./assets/image/flow/title2.png" alt="">
                    </div>
                    <div class="c-step__inner">
                        <div class="c-imgText1 c-imgText1--reverse">
                            <div class="c-imgText1__img">
                                <img src="./assets/image/flow/step2.jpg" alt="">
                            </div>
                            <div class="c-imgText1__content">
                                家族の顔が見える家、趣味を生かした家、ペットと<br/>
                                暮らしやすい家、シンプルなデザインの家、窓の大<br/>
                                きい家など、世界に一つしかないお客様の建てたい<br/>
                                家のイメージや想いをたくさんお聞かせください。
                            </div>
                        </div>
                    </div>
                </div>
                <div class="c-step__item">
                    <div class="c-step__title u-right">
                        <img src="./assets/image/flow/title3.png" alt="">
                    </div>
                    <div class="c-step__inner">
                        <div class="c-imgText1">
                            <div class="c-imgText1__img">
                                <img src="./assets/image/flow/step3.jpg" alt="">
                            </div>
                            <div class="c-imgText1__content">
                                できあたってきたイメージをもと耐震・耐熱・防犯などの<br/>
                                機能性も考慮しながら、アウトラインからでティールまで<br/>
                                <span>カタチを作っていきます。</span><br/>
                                いよいよ、お客様個々の住まいが図面となり、平面上に絵<br/>
                                となって浮かび上がります。
                            </div>
                        </div>
                    </div>
                </div>
                <div class="c-step__item">
                    <div class="c-step__title u-right">
                        <img src="./assets/image/flow/title4.png" alt="">
                    </div>
                    <div class="c-step__inner">
                        <div class="c-imgText1 c-imgText1--reverse">
                            <div class="c-imgText1__img">
                                <img src="./assets/image/flow/step4.jpg" alt="">
                            </div>
                            <div class="c-imgText1__content">
                                近隣へと挨拶と鎮魂祭から始まります。<br/>
                                家の大きさにもよりますが、約3か月から5か<br/>
                                月間、各業種のエキスパートが住まいづくりに関<br/>
                                わります。各工程の記録、検査を経てオンリーワ<br/>
                                ンの住まいが建ち上がります。
                            </div>
                        </div>
                    </div>
                </div>
                <div class="c-step__item">
                    <div class="c-step__title u-right">
                        <img src="./assets/image/flow/title5.png" alt="">
                    </div>
                    <div class="c-step__inner">
                        <div class="c-imgText1">
                            <div class="c-imgText1__img">
                                <img src="./assets/image/flow/step5.jpg" alt="">
                            </div>
                            <div class="c-imgText1__content">
                                完成後、竣工検査などを経て、晴れてお客様へとお引き渡<br/>
                                しいたします。ついに、お客様だけの住まいの完成です。<br/>
                                住み始めてからの不具合や、修繕、増築、改築なども、も<br/>
                                ちろんお気軽にご相談ください。<br/>
                                末長いお付き合いをお願いいたします。
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- p-flow3 -->
            <section class="p-flow3">
                <img src="./assets/image/flow/title6.png" alt="" class="c-cost__title">
                <div class="c-cost__content">
                    <p>設計事務所の住まいづくりは、決して高くはありません。</p>
                    <p>よく、「設計事務所に依頼すると、設計費が余分にかかるんじゃないの？」と言われますが、決してそうではありません。ハウスメーカー、工務店、大工さんに依頼する場合も、設計費という項目がない会社もありますが、必ず設計費はかかっているのです。</p>
                    <p>設計事務所から工務店に依頼する場合、当然、設計費を抜いた金額で依頼する為、同じ建物を建てるならば、設計施工の会社に依頼する場合と比べて高くなるということはありません。</p>
                    <p>もちろん、条件が難しかったり、特別なこだわりがある方の場合は、工事も含めそれなり費用になりますが、そういった場合が設計事務所としての腕の見せ所だと思っていますし、得意としてます。</p>
                </div>
            </section>
            <!-- p-flow4 -->
            <section class="p-flow4">
                <img src="./assets/image/flow/title7.png" alt="" class="c-estimate__title">
                <div class="c-estimate__content">
                    <p>
                        計画建物の大きさや、敷地、申請、用途などの条件によってさまざまですが、<br/>
                        おおよそ、建築工事費の6％～10％です。<br/>
                        建物が大きいほど、スケールメリットがでて、設計費も安くなります。
                        </p>
                    <p>30～40坪で、基本計画、実施設計、設計監理、外構計画、インテリアコーディネートなど、全て含んで、工事金額の8％くらいが目安となります。 </p>
                </div>
            </section>
            <!-- p-flow5 -->
            <section class="p-flow5">
                <div class="c-contact">
                    <div class="c-contact__text">
                        <p>お問い合わせ・ご相談はお気軽に</p>
                        <p>090-5870-0000</p>
                        <p>受付時間　10：00〜20：00　◯曜定休</p>
                    </div>
                    <div class="c-contact__img">
                        <a href="tel:09058700000">
                            <div class="c-contact__inner">
                                <p>Contact</p>
                                <img src="./assets/image/top/mail-ft.png" alt="">
                                <p>
                                    お問い合わせ<br/>
                                    フォーム
                                </p> 
                            </div>
                        </a>
                    </div>
                </div>
            </section>
        </div>
    </main>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>