<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/meta.php'); ?>
<link href="/assets/js/slick/slick.css" rel="stylesheet">
<link href="/assets/js/slick/slick-theme.css" rel="stylesheet">
<link href="/assets/css/style.css" rel="stylesheet">
<script src="/assets/js/jquery-2.2.4.min.js"></script>
<script src="/assets/js/slick/slick.js"></script>
<script src="/assets/js/jquery.matchHeight-min.js"></script>
<script src="/assets/js/functions.js"></script>
</head>
<body class="page-<?php echo $id; ?>">
<header>
    <div class="c-header">
        <div class="c-header__menu">
            <div class="c-header__icon">
                <a href="/">
                    <p>一級建築士事務所 <span class="u-c1">シープ</span></p>
                    <p>SHE<span class="u-c1">a</span>P</p>
                </a>
            </div>
            <div class="c-header__text1">日々をたのしむ住まい。</div>
            <nav class="c-navi1">
                <ul>
                    <li><a href="./qa.php">Q&A</a></li>
                    <li><a href="#">ブログ</a></li>
                    <li><a href="./flow.php">住まいづくりのすすめ方</a></li>
                    <li><a href="#">ギャラリー</a></li>
                    <li><a href="#">デザインコンセプト</a></li>
                    <li><a href="#">シープについて</a></li>
                </ul>
            </nav>
            <div class="c-header__img">
                <img src="./assets/image/top/book-hd.png" alt="">
            </div>
            <div class="c-header__text2">
                <p>サイトマップ</p>
                <p>プライバシーポリシー</p>
            </div>
            <div class="c-header__home">
                <a href="/">
                    <img src="./assets/image/top/text1.png" alt="">
                </a>
            </div>
        </div>
        <div class="c-header__contact">
            <a href="tel:0905870000">
                <p>Contact</p>
                <img src="./assets/image/top/mail-hd.png" alt="">
                <p>お問い合わせ</p>
            </a>
        </div>
    </div>
</header>