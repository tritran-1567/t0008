<footer>
    <div class="c-footer">
        <div class="c-footer__taskbar">
            <div class="c-footer__home">
                <a href="/">
                    <img src="./assets/image/top/icon3.png" alt="">
                    ホーム
                </a>
            </div>
            <div class="c-footer__scrolltop">
                ページトップへ
            </div>
        </div>
        <div class="c-list1">
            <div class="c-list1__col">
                <div class="c-title4">
                    <img src="./assets/image/top/ft1.png" alt="">
                    <span class="u-pt5 u-pr13">シープについて</span>
                </div>
                <img src="./assets/image/top/line4.png" alt="" class="u-mt4 u-ml-1">
                <ul class="u-mt3 u-mb26">
                    <li><a href="#">設計事務所と創りましょう</a></li>
                    <li><a href="#">事務所概要</a></li>
                    <li><a href="#">プロフィール</a></li>
                    <li><a href="#">アクセスマップ</a></li>
                </ul>
                <div class="c-title4 u-pl1">
                    <img src="./assets/image/top/ft2.png" alt="">
                    <span class="u-pr13">ギャラリー</span>
                </div>
                <img src="./assets/image/top/line4.png" alt="" class="u-mb32 u-ml-1">
                <div class="c-title4">
                    <img src="./assets/image/top/ft3.png" alt="" class="u-pl1">
                    <span class="u-pt5 u-pr13">住まいづくりのすすめ方</span>
                </div>
                <img src="./assets/image/top/line4.png" alt="" class="u-mt4 u-ml-1">
            </div>
            <div class="c-list1__col">
                <div class="c-title4">
                    <img src="./assets/image/top/ft4.png" alt="" class="u-pt1 u-pl2">
                    <span class="u-mt-1">デザインコンセプト</span>
                </div>
                <img src="./assets/image/top/line4.png" alt="" class="u-mt-1">
                <ul>
                    <li><a href="#">光</a></li>
                    <li><a href="#">風</a></li>
                    <li><a href="#">省エネ</a></li>
                    <li><a href="#">安心・安全</a></li>
                    <li><a href="#">広がり・つながり</a></li>
                    <li><a href="#">シンプル</a></li>
                </ul>
                <div class="c-title4">
                    <img src="./assets/image/top/ft5.png" alt="" class="u-pl2">
                    <span>お問い合わせ</span>
                </div>
                <img src="./assets/image/top/line4.png" alt="" class="u-mt4">
            </div>
            <div class="c-list1__col">
                <div class="c-title4">
                    <img src="./assets/image/top/ft6.png" alt="" class="u-pb4 u-pl2">
                    <span>質問と解答</span>
                </div>
                <img src="./assets/image/top/line4.png" alt="" class="u-mb34">
                <div class="c-title4">
                    <img src="./assets/image/top/ft7.png" alt="" class="u-pl2">
                    <span>ブログ</span>
                </div>
                <img src="./assets/image/top/line4.png" alt="" class="u-mt-1">
                <div class="c-footer__security">
                    <p><a href="#">プライバシーポリシー</a></p>
                    <p><a href="#">サイトマップ</a></p>
                </div>
            </div>
            <div class="c-list1__col">
                <img src="./assets/image/top/line3.png" alt="">
                <div class="c-list1__social">
                    <a href="#">
                        <img src="./assets/image/top/icon1.png" alt="">
                        <span>公式Facebookはこちら</span>
                    </a>
                </div>
                <img src="./assets/image/top/line3.png" alt="">
                <div class="c-list1__social">
                    <a href="#">
                        <img src="./assets/image/top/icon2.png" alt="">
                        <span>ひつじ的住まいズム！</span>
                    </a>
                </div>
                <img src="./assets/image/top/line3.png" alt="">
                <div class="c-list1__social">
                    <a href="#">
                        <img src="./assets/image/top/icon2.png" alt="">
                        <span>ヒツジ的フォトライフ！</span>
                    </a>
                </div>
                <img src="./assets/image/top/line3.png" alt="">
            </div>
        </div>
    </div>
    <div class="c-footer__mid">
        <div class="c-footer__icon">
            <p>一級建築士事務所 <span class="u-c1">シープ</span></p>
            <img src="./assets/image/top/text-ft.png" alt="">
        </div>
        <div class="c-footer__address">
            <p>一級建築士事務所登録　静岡県知事登録（◯-00）第00000号</p>
            <p>〒400-0000静岡県浜松市中区三組町28-76</p>
            <p>TEL：053-000-0000　FAX：053-000-0000</p>
        </div>
        <a href="#" class="c-footer__fb"><img src="./assets/image/top/icon4.png" alt=""></a>
    </div>
    <img src="./assets/image/top/ft-sub.png" alt="" class="c-footer__sub">
</footer>
<script src="/assets/js/functions.js"></script>
</body>
</html>