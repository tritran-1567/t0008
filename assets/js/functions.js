/*===================================================
Viewport width fit for Tablet
===================================================*/
var _ua = (function(u){
  return {
    Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
      || u.indexOf("ipad") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
      || u.indexOf("kindle") != -1
      || u.indexOf("silk") != -1
      || u.indexOf("playbook") != -1,
    Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
      || u.indexOf("iphone") != -1
      || u.indexOf("ipod") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
      || u.indexOf("blackberry") != -1
  }
})(window.navigator.userAgent.toLowerCase());

if(_ua.Tablet){
  $("meta[name='viewport']").attr('content', 'width=1100');
}

$(document).ready(function(){

  $(".c-slide1").slick({
    arrows: false,
    dots: true,
    autoplay: true,
    autoplaySpeed: 2000
  });

  $(".c-footer__scrolltop").click(function(){
    $("html, body").animate({scrollTop: 0}, 600);
  });

  $(".c-form1__radio").click(function(){
    if(!$(this).hasClass("active")){
      $(".c-form1__radio").removeClass("active");
      $(this).addClass("active");
    }
  });

  if($("body").hasClass("page-flow")){
    $(".c-footer").css("padding","0px 0 61px");
    $(".c-navi1").find("a[href='./flow.php']").addClass("active");
    $(window).scroll(function(){
      if($(window).scrollTop() + $(window).height() > $(document).height() - 400){
        $(".c-footer__sub").addClass("active");
      }else{
        $(".c-footer__sub").removeClass("active");
      }
      if($(window).scrollTop() + $(window).height() > $(document).height() - 1000){
        $(".c-header__menu").css("transition","all .75s");
        $(".c-header__contact").css("transition","all .75s");
        $(".c-header__menu").css("opacity","0");
        $(".c-header__contact").css("opacity","0");
      }else{
        $(".c-header__menu").css("opacity","1");
        $(".c-header__contact").css("opacity","1");
      }
    })
  }
  else if($("body").hasClass("page-index")){
    var s = $(".c-form1__wrapper");
    var s1 = $("#stop_contact");
    var offset = s.offset();
    var offset1 = s1.offset();
    var p1 = -1;
    var p3 = 46;
    $(window).scroll(function(){
      if($(window).scrollTop() + $(window).height() > offset.top + 450){
        $(".c-header__menu").css("position","absolute");
        $(".c-header__menu").css("top",offset.top-510);
      }else{
        $(".c-header__menu").css("position","fixed");
        $(".c-header__menu").css("top",p1);
      }
      if($(window).scrollTop() + $(window).height() > offset1.top + 850){
        $(".c-header__contact").css("position","absolute");
        $(".c-header__contact").css("top",offset1.top-100);
      }else{
        $(".c-header__contact").css("position","fixed");
        $(".c-header__contact").css("top",p3);
      }
      if($(window).scrollTop() + $(window).height() > $(document).height() - 300){
        $(".c-footer__sub").addClass("active");
      }else{
        $(".c-footer__sub").removeClass("active");
      }
    });
  }else{
    $(".c-navi1").find("a[href='./qa.php']").addClass("active");
    $(".c-qa__inner").hide();
    $(".c-qa__box:nth-child(2)").find(".c-qa__inner").addClass("active");
    $(".c-qa__question").click(function(){
      if($(".c-qa__question").hasClass("active")){
        $(".c-qa__question").removeClass("active");
        $(".c-qa__inner").removeClass("active");
        $(".c-qa__inner").slideUp();
      }else{
        $(this).addClass("active");
        $(this).next().addClass("active");
      }
    });
    $(".c-qa__question").dblclick(function(e){
      e.preventDefault();
    })

    $(window).scroll(function(){
      if($(window).scrollTop() + $(window).height() > $(document).height() - 400){
        $(".c-footer__sub").addClass("active");
      }else{
        $(".c-footer__sub").removeClass("active");
      }
      if($(window).scrollTop() + $(window).height() > $(document).height() - 1000){
        $(".c-header__menu").css("transition","all .75s");
        $(".c-header__contact").css("transition","all .75s");
        $(".c-header__menu").css("opacity","0");
        $(".c-header__contact").css("opacity","0");
      }else{
        $(".c-header__menu").css("opacity","1");
        $(".c-header__contact").css("opacity","1");
      }
    })
  }
});