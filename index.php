<?php $id="index";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<div class="p-top">
	<main>
		<!-- p-top1 -->
		<section class="p-top1">
			<div class="c-desc">
				<span>シープは静岡、愛知、岐阜、三重、を中心とした一級建築士事務所です。</span>
			</div>
			<div class="c-slide1">
				<?php for($i = 0; $i < 5; $i++) { ?>
				<div class="c-slide1__item">
					<img src="./assets/image/top/slide1.jpg" alt="">
				</div>
				<?php } ?>
			</div>
		</section>
		<!-- p-top2 -->
		<section class="p-top2">
			<div class="c-intro u-center">
				<div class="c-intro__title">
					スタイリッシュで<br/>
					人と地球に優しい住まいづくり
				</div>
				<div class="c-intro__content">
					豊かな住まいとは、ただ広く、豪華な素材を使うことではありません <br/>
					本当の意味での「豊かな住まい」とは 住まう人に合っていることなのではないでしょうか<br/>
					じっくりと話し、ライフスタイルにフィットしたものを見つけ出していく<br/>
					一級建築士事務所 SHEaPは、そんな設計を心がけています 
				</div>
				<img src="./assets/image/top/img1.png" alt="" class="c-intro__img">
			</div>
		</section>
		<!-- p-top3 -->
		<section class="p-top3">
			<div class="c-about">
				<div class="c-about__img">
					<img src="./assets/image/top/img3.png" alt="">
				</div>
				<div class="c-about__content">
					<div class="c-title3">
						こんにちは、<br/>
						一級建築士事務所<span>SHEaP</span>の<br/>
						戸塚治夫です。
					</div>
					<p>住まいづくりをする方が一番意識するのは、間取りや材料、デザイン、そして、コストとのバランスです。それを総合的に提案するのが設計者の役割だと考えています。</p>
					<p>せっかくの住まいづくりですから、より住まう自分達にあったものを追求して欲しいと思います。</p>
					<p>住まう方にフィットする住まいを提供する為に、お客様とのコミュニケーションを大切にして設計したいと思っています。 </p>
				</div>
				<a href="#"><img src="./assets/image/top/sheep1.png" alt="" class="c-about__sheep"></a>
			</div>
		</section>
		<div class="l-container">
			<div class="l-row">
				<!-- p-top4 -->
				<section class="p-top4">
					<div class="c-title1">
						<ul>
							<li>Des<span class="u-c2">i</span>ng</li>
							<li class="u-c2">6つの設計コンセプト</li>
						</ul>
					</div>
					<div class="c-imgText1">
						<div class="c-imgText1__text">
							<div class="c-title3">
								SHEaPでは、<br/>
								6つの設計コンセプトを<br/>
								大切にしています。
							</div>
							<p>
								一級建築士事務所SHEaPでは、<br/>
								「光」「風」「省エネ」「安心・安全」「広がり・つながり」「シンプル」といった6つの設計コンセプトを大切に考えています。
							</p>
						</div>
						<div class="c-imgText1__img">
							<img src="./assets/image/top/img4.png" alt="">
						</div>
						<a href="#"><img src="./assets/image/top/sheep2.png" alt="" class="c-desing__sheep"></a>
					</div>
				</section>
				<!-- p-top5 -->
				<section class="p-top5">
					<div class="c-title1">
						<ul>
							<li>G<span class="u-c1">a</span>llery</li>
							<li class="u-c1">設計事例と提案事例</li>
						</ul>
					</div>
					<div class="c-gallery__sub">SHEaPのお仕事の一部をご覧ください。</div>
					<div class="c-list1">
						<?php for($i=0;$i<11;$i++) {?>
							<?php if($i%2==0) {?>
								<a href="#"><img src="./assets/image/top/gal2.jpg" alt=""></a>
							<?php } else { ?>
								<a href="#"><img src="./assets/image/top/gal1.jpg" alt=""></a>
							<?php } ?>
						<?php } ?>
					</div>
					<a href="#"><img src="./assets/image/top/sheep1.png" alt="" class="c-gallery__img"></a>
				</section>
				<!-- p-top6 -->
				<section class="p-top6">
					<div class="c-title1">
						<ul>
							<li>Sched<span class="u-c2">u</span>le</li>
							<li class="u-c2">住まいづくりのすすめ方</li>
						</ul>
					</div>
					<div class="c-schedule__sub">
						<p>住まいづくりの流れをご紹介しています。</p>
						<span>この限りではありませんので、ご要望に合わせてご相談ください。</span>
					</div>
					<div class="c-list1">
						<div class="c-list1__img">
							<img src="./assets/image/top/schedule1.jpg" alt="">
						</div>
						<div class="c-list1__text">
							<div class="c-title2">
								<ul>
									<li><span class="u-c2">1.</span>聞く</li>
									<li class="u-c2">基本計画段階 1.5〜2ヶ月</li>
								</ul>
							</div>
							<img src="./assets/image/top/line1.png" alt="">
							<p class="u-c2">お引き合い・打ち合せ</p>
							<p>・現在の悩みと「こうなりたい」のイメージ</p>
							<p>・敷地の調査</p>
							<p>・法規チェック</p>
							<img src="./assets/image/top/line2.png" alt="">
							<p class="u-c2">プレゼンテーション</p>
							<p>・基本計画図（配置図・平面図・立面図）</p>
							<p>・イメージスケッチ等</p>
							<p>・設計業務 概算のお見積もり	</p>				
						</div>
					</div>
					<div class="c-list1">
						<div class="c-list1__img">
							<img src="./assets/image/top/schedule2.jpg" alt="">
						</div>
						<div class="c-list1__text">
							<div class="c-title2">
								<ul>
									<li><span class="u-c2">2.</span>考える</li>
									<li class="u-c2">設計段階 3〜6ヶ月</li>
								</ul>
							</div>
							<img src="./assets/image/top/line1.png" alt="">
							<p class="u-c2">お引き合い・打ち合せ</p>
							<p>・現在の悩みと「こうなりたい」のイメージ</p>
							<p>・敷地の調査</p>
							<p>・法規チェック</p>
							<img src="./assets/image/top/line2.png" alt="">
							<p class="u-c2">プレゼンテーション</p>
							<p>・基本計画図（配置図・平面図・立面図）</p>
							<p>・イメージスケッチ等</p>
							<p>・設計業務 概算のお見積もり	</p>				
						</div>
					</div>
					<div class="c-list1">
						<div class="c-list1__img">
							<img src="./assets/image/top/schedule3.jpg" alt="">
						</div>
						<div class="c-list1__text">
							<div class="c-title2">
								<ul>
									<li><span class="u-c2">3.</span>つくる</li>
									<li class="u-c2">監理段階 監理7〜8ヶ月</li>
								</ul>
							</div>
							<img src="./assets/image/top/line1.png" alt="">
							<p class="u-c2">お引き合い・打ち合せ</p>
							<p>・現在の悩みと「こうなりたい」のイメージ</p>
							<p>・敷地の調査</p>
							<p>・法規チェック</p>
							<img src="./assets/image/top/line2.png" alt="">
							<p class="u-c2">プレゼンテーション</p>
							<p>・基本計画図（配置図・平面図・立面図）</p>
							<p>・イメージスケッチ等</p>
							<p>・設計業務 概算のお見積もり	</p>				
						</div>
					</div>
					<div class="c-list1 c-list1--last">
						<div class="c-list1__img">
							<img src="./assets/image/top/schedule4.jpg" alt="">
						</div>
						<div class="c-list1__text">
							<div class="c-title2">
								<ul>
									<li><span class="u-c2">4.</span>住まう</li>
									<li class="u-c2">お引渡し</li>
								</ul>
							</div>
							<img src="./assets/image/top/line1.png" alt="">
							<p class="u-c2">お引き合い・打ち合せ</p>
							<p>・現在の悩みと「こうなりたい」のイメージ</p>
							<p>・敷地の調査</p>
							<p>・法規チェック</p>
							<img src="./assets/image/top/line2.png" alt="">
							<p class="u-c2">プレゼンテーション</p>
							<p>・基本計画図（配置図・平面図・立面図）</p>
							<p>・イメージスケッチ等</p>
							<p>・設計業務 概算のお見積もり	</p>				
						</div>
					</div>
					<a href="./flow.php"><img src="./assets/image/top/sheep3.png" alt="" class="c-schedule__sheep"></a>
				</section>
				<!-- p-top7 -->
				<section class="p-top7">
					<div class="c-thumb">
						<div class="c-title1">
							<ul>
								<li>BLOG</li><br/>
								<li class="u-c1">ヒツジ的住まいズム！</li>
							</ul>
						</div>
						<div class="c-thumb__content">
							<div class="c-thumb__inner">
								<p>タイトル</p>
								<p>　記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。</p>
								<p>　記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。<br/>記事内容がここに表示されます。記事内容がここに表示されます。</p>
							</div>
						</div>
						<a href="#"><img src="./assets/image/top/sheep1.png" alt="" class="c-thumb__sheep"></a>
					</div>
				</section>
				<!-- p-top8 -->
				<section class="p-top8">
					<div class="c-title1">
						<ul>
							<li>Q<span class="u-c2">&</span>A</li>
							<li class="u-c2">よく寄せられる質問</li>
						</ul>
					</div>
					<a href="#"><img src="./assets/image/top/sheep4.png" alt="" class="c-qa__sheep"></a>
					<div class="c-list1">
						<div class="c-list1__box">
							<a href="./qa.php">
								<img src="./assets/image/top/img2.png" alt="">
								<div class="c-list1__title">Q1.</div>
								<div class="c-list1__content">
									設計事務所って<br/>
									高いんじゃないの？
								</div>
							</a>
						</div>
						<div class="c-list1__box">
							<a href="./qa.php">
								<img src="./assets/image/top/img2.png" alt="">
								<div class="c-list1__title">Q2.</div>
								<div class="c-list1__content">
									工事は請け負わないの？
								</div>
							</a>
						</div>
						<div class="c-list1__box">
							<a href="./qa.php">
								<img src="./assets/image/top/img2.png" alt="">
								<div class="c-list1__title">Q3.</div>
								<div class="c-list1__content">
									小さな会社に依頼しても<br/>
									安心できますか？
								</div>
							</a>
						</div>
						<div class="c-list1__box">
							<a href="./qa.php">
								<img src="./assets/image/top/img2.png" alt="">
								<div class="c-list1__title">Q4.</div>
								<div class="c-list1__content">
									設計事務所って<br/>
									高いんじゃないの？
								</div>
							</a>
						</div>
						<div class="c-list1__box">
							<a href="./qa.php">
								<img src="./assets/image/top/img2.png" alt="">
								<div class="c-list1__title">Q5.</div>
								<div class="c-list1__content">
									工事は請け負わないの？
								</div>
							</a>
						</div>
						<div class="c-list1__box">
							<a href="./qa.php">
								<img src="./assets/image/top/img2.png" alt="">
								<div class="c-list1__title">Q6.</div>
								<div class="c-list1__content">
									小さな会社に依頼しても<br/>
									安心できますか？
								</div>
							</a>
						</div>
					</div>
				</section>
				<!-- p-top9 -->
				<section class="p-top9" id="stop_contact">
					<div class="c-title1">
						<ul>
							<li>Contact</li>
							<li class="u-c2">お問い合わせ</li>
						</ul>
					</div>
					<div class="c-contact__sub">
						住まいづくり・建築計画に係る相談を承っております。<br/>
						相談は無料です。気軽にお声かけください。 
					</div>
					<div class="c-form1">
						<form action="" method="post">
							<div class="c-form1__title">お名前<span class="c-form1__warning u-c2">【必須】</span></div>
							<input type="text" name="text" class="c-form1__name">
							<div class="c-form1__title">Email<span class="c-form1__warning u-c2">【必須】</span></div>
							<input type="text" name="text" class="c-form1__email">
							<div class="c-form1__title">お問い合わせ種別</div>
							<ul>
								<li>
									<label class="c-form1__radio">
										<input type="radio" name="radio">
										<span>新築</span>									
									</label>
								</li>
								<li>
									<label class="c-form1__radio">
										<input type="radio" name="radio">
										<span>リフォーム</span>									
									</label>
								</li>
								<li>
									<label class="c-form1__radio">
										<input type="radio" name="radio">
										<span>アパート・マンションオーナー様</span>									
									</label>
								</li>
								<li>
									<label class="c-form1__radio">
										<input type="radio" name="radio">
										<span>相談会申込み</span>									
									</label>
								</li>
								<li>
									<label class="c-form1__radio">
										<input type="radio" name="radio">
										<span>その他</span>									
									</label>
								</li>
							</ul>
							<div class="c-form1__title">郵便番号</div>
							<input type="text" name="text" class="c-form1__zip">
							<div class="c-form1__title">ご住所</div>
							<input type="text" name="text" class="c-form1__address">
							<div class="c-form1__title">お電話番号</div>
							<input type="text" name="text" class="c-form1__tel">
							<div class="c-form1__title u-mt-5">お問い合わせ内容<span class="c-form1__warning u-c2">【必須】</span></div>
							<textarea name="content" id="c-form1__content" class="c-form1__content" cols="30" rows="12"></textarea>
							<div class="c-form1__wrapper">
								<div class="c-btn1">
									<a href="#">リセット</a>
								</div>
								<div class="c-btn1">
									<a href="#">確認画面へ</a>
								</div>
							</div>
						</form>
					</div>
					<div class="c-contact">
						<div class="c-contact__text">
							<p>お問い合わせ・ご相談はお気軽に</p>
							<p>090-5870-0000</p>
							<p>受付時間　10：00〜20：00　◯曜定休</p>
						</div>
						<div class="c-contact__img">
							<a href="tel:09058700000">
								<div class="c-contact__inner">
									<p>Contact</p>
									<img src="./assets/image/top/mail-ft.png" alt="">
									<p>
										お問い合わせ<br/>
										フォーム
									</p> 
								</div>
							</a>
						</div>
					</div>
				</section>
			</div>
		</div>
	</main>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>